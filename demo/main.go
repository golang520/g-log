package main

import (
	"gitee.com/golang520/g-log/g"
	"os"
)

func init() {
	builder := g.NewLogUtilsBuilder("DEBUG", "test.log", 10, 5, 3, true, true)
	logUtils := g.NewLogUtils().SetBuilder(builder)
	err := logUtils.Init()
	if err != nil {
		os.Exit(1)
	}
}
func main() {
	g.Log().Info("测试info")
	g.Log().Debug("测试debug")
	g.Log().Warn("测试warn")
	g.Log().Error("测试error")

}
