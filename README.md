### g-log日志库

### 依赖如下组件

- go.uber.org/zap/zapcore
- gopkg.in/natefinch/lumberjack.v2
- github.com/fatih/color


### 日志功能介绍
- 日志有自动切割
- 日志保留大小设置
- 日志自动保存时间
- 日志保存的份数自己设置


### 具体用法
```ssh
go get -u gitee.com/golang520/g-log@latest
```
--- 
### 参数说明
 - 参数1:日志级别设置
 - 参数2:日志文件的名字
 - 参数3:日志大小限制,单位MB
 - 参数4:日志文件保留天数
 - 参数5:最大保留历史日志数量,其实就是备份数量
 - 参数6:日志打印地方,true打印到控制,false写入到文件中
 - 参数7:是否显示打印的行号

```go
func init() {
    builder := g.NewLogUtilsBuilder("DEBUG", "test.log", 10, 5, 3, true,true)
    logUtils := g.NewLogUtils().SetBuilder(builder)
    err := logUtils.Init()
    if err != nil {
        os.Exit(1)
    }
}
```
### 代码中使用
```go
func main() {
    g.Log().Info("test")
    g.Log().Error("errors")
    g.Log().Debug("debug")
    g.Log().Warn("warn")
}
```

#### 写入文件的日志
![](.README_images/296516fc.png)

#### 控制台打印的日志
![](.README_images/c145dda6.png)
